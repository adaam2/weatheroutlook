﻿using WeatherOutlookLondon.Data.Repositories;
using WeatherOutlookLondon.Data.Interfaces;
using WeatherOutlookLondon.Models;
using System.Web.Mvc;
using WeatherOutlookLondon.Data.Seed;
using System.Threading.Tasks;
using System.Configuration;

namespace WeatherOutlookLondon.Controllers
{
    public class HomeController : AsyncController
    {
        private readonly ICrudRepository<Forecast> _repository;

        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["main"].ConnectionString;

        public HomeController(ICrudRepository<Forecast> repository)
        {
            _repository = repository;
        }

        public ActionResult Index(bool seeded = false)
        {
            if (seeded)
            {
                ViewBag.seeded = true;
            }

            ViewBag.highAverage = _repository.GetAverageForPreceedingWeeks(Temperature.High);
            ViewBag.lowAverage = _repository.GetAverageForPreceedingWeeks(Temperature.Low);

            return View();
        }

        public async Task<JsonResult> Forecast()
        {
            var days = await _repository.All();
            return Json(days, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Seed()
        {
            Seeder seeder = new Seeder(_connectionString);

            await seeder.SeedDatabaseAsync();

            return RedirectToAction("Index", "Home", new { seeded = true });
        }
    }
}