﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherOutlookLondon.Models
{
    public class Forecast
    {
        [JsonIgnore]
        public int ID { get; set; }
        public string DayName { get; set; }
        [JsonIgnore]
        public DateTime Date { get; set; }
        public string DateStr { get; set; }
        public string Description { get; set; }
        public int HighTemp { get; set; }
        public int LowTemp { get; set; }
        public int AverageDifference { get; set; }
    }
}
