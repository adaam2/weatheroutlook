﻿using System.Web.Optimization;
using BundleTransformer.Core.Builders;
using BundleTransformer.Core.Transformers;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Resolvers;

namespace WeatherOutlookLondon
{
    public class BundleConfig
    {
        /// <summary>
        /// Using BundleTransformer, we can run a LibSass host on the fly to compile SCSS and minify it into a bundle.
        /// </summary>
        /// <param name="bundles"></param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Setup all of the (overly verbose) transforms included in the BundleTransformer library
            var builder = new NullBuilder();
            var styleTransformer = new StyleTransformer();
            var orderer = new NullOrderer();

            BundleResolver.Current = new CustomBundleResolver();

            // Create a new style bundle
            var styleBundle = new StyleBundle("~/bundles/styles").Include("~/Styles/main.scss");
            styleBundle.Builder = builder;
            styleBundle.Transforms.Add(styleTransformer);

            // Add CSS minification to the transforms
            styleBundle.Transforms.Add(new CssMinify());
            styleBundle.Orderer = orderer;

            bundles.Add(styleBundle);


            var scriptBundle = new ScriptBundle("~/bundles/scripts").Include("~/Scripts/main.jsx");
            bundles.Add(scriptBundle);

            BundleTable.EnableOptimizations = false;            
        }
    }
}
