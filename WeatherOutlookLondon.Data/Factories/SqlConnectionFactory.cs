﻿using System;
using System.Data;
using System.Data.SqlClient;
using WeatherOutlookLondon.Data.Interfaces;

namespace WeatherOutlookLondon.Data.Factories
{
    public class SqlConnectionFactory : IDbConnectionFactory
    {
        private readonly string _connectionString;

        private SqlConnectionFactory() { }

        public SqlConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IDbConnection CreateConnection()
        {
            var connection = new SqlConnection(_connectionString);
            connection.Open();
            return connection;
        }
    }
}
