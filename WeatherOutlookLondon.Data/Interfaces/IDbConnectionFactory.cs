﻿using System.Data;

namespace WeatherOutlookLondon.Data.Interfaces
{
    public interface IDbConnectionFactory
    {
        IDbConnection CreateConnection();
    }
}
