﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WeatherOutlookLondon.Data.Interfaces;
using WeatherOutlookLondon.Models;
using Dapper;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Newtonsoft;
using System.Net;
using System.IO;
using System.Xml;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Globalization;

namespace WeatherOutlookLondon.Data.Repositories
{
    public class ForecastRepository : ICrudRepository<Forecast>
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;


        public ForecastRepository(IDbConnectionFactory dbConnectionFactory)
        {
            this._dbConnectionFactory = dbConnectionFactory;
        }

        #region Basic CRUD methods that act on the table Forecast
        public async Task Add(Forecast obj)
        {
            const string sql = "INSERT INTO dbo.Forecast(Date, Description, HighTemp, LowTemp, DayName, DateStr) VALUES(@date, @desc, @high, @low, @dayName, @dateStr)";

            await WithConnection(async c =>
            {
                await c.ExecuteAsync(sql, new { date = obj.Date, desc = obj.Description, high = obj.HighTemp, low = obj.LowTemp, dayName = obj.DayName, dateStr = obj.DateStr });
            });
        }

        public int GetAverageForPreceedingWeeks(Temperature temperature)
        {
            string fieldName = string.Empty;

            switch (temperature)
            {
                case Temperature.Low:
                    fieldName = "LowTemp";
                break;
                case Temperature.High:
                    fieldName = "HighTemp";
                    break;
                default:
                    throw new ApplicationException("Unrecognized temperature type provided");
            }

            string sql = string.Format("SELECT AVG({0}) as AverageTemp FROM dbo.Forecast WHERE Forecast.[Date] < GETDATE()", fieldName);

            int number;

            using(var conn = _dbConnectionFactory.CreateConnection())
            {
                number = conn.Query<int>(sql).FirstOrDefault();
            }

            return number;
        }

        public async Task<ICollection<Forecast>> All()
        {
            string sql = "SELECT * FROM dbo.Forecast WHERE date BETWEEN @today AND @threeDaysTime";

            DateTime today = DateTime.Today;

            DateTime threeDaysTime = DateTime.Today.AddDays(3);

            return await WithConnection(async c =>
		    {
                var forecasts = await c.QueryAsync<Forecast>(sql, new { today = today, threeDaysTime = threeDaysTime });

                return forecasts.ToList();
		    });
        }

        public async Task<Forecast> Find()
        {
            throw new NotImplementedException();
        }

        public async Task<ICollection<Forecast>> FindWhere(Expression<Func<Forecast, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task Update(Forecast obj)
        {
            string sql = "UPDATE dbo.Forecast SET Date = @date, Description = @desc, HighTemp = @high, LowTemp = @low WHERE ID = @id";

            using (var conn = _dbConnectionFactory.CreateConnection())
            {
                await conn.ExecuteAsync(sql, new { date = obj.Date, desc = obj.Description, high = obj.HighTemp, low = obj.LowTemp });
            }
        }
        #endregion

        #region Retrieving data from weather.com
        public async Task<ICollection<Forecast>> RetrieveFutureForecasts()
        {
            const string contentType = "application/xml";
            const string endpoint = "http://wxdata.weather.com/wxdata/weather/local/UKXX0085?cc=*&unit=m&dayf=3";

            var xml = await MakeAsyncRequest(endpoint, contentType);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            string json = JsonConvert.SerializeXmlNode(doc);

            RootObject root = null;

            await Task.Factory.StartNew(() =>
            {
                root = JsonConvert.DeserializeObject<RootObject>(json);
            });

            var days = root.weather.dayf.day.ToList();

            string baseDayStr = root.weather.dayf.lsup.Replace(" BST", string.Empty);
            DateTime baseDay = DateTime.Parse(baseDayStr, CultureInfo.GetCultureInfo("en-US"));

            // Now let's put these values into our own models
            ICollection<Forecast> forecasts = new List<Forecast>();

            foreach(Day day in days)
            {
                var dateToUse = baseDay.AddDays(int.Parse(day.d));

                forecasts.Add(new Forecast()
                {
                    Date = dateToUse,
                    DayName = day.t,
                    Description = day.part.First().t,
                    HighTemp = int.Parse(day.hi),
                    LowTemp = int.Parse(day.low),
                    DateStr = dateToUse.ToString("dd/MM/yyyy")
                });


            }

            return forecasts;          
        }

        public async Task<string> MakeAsyncRequest(string url, string contentType)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = contentType;
            request.Method = WebRequestMethods.Http.Get;
            request.Timeout = 20000;
            request.Proxy = null;

            Task<WebResponse> task = Task.Factory.FromAsync(
                request.BeginGetResponse,
                asyncResult => request.EndGetResponse(asyncResult),
                (object)null);

            return await ReadStreamFromResponse(task.Result);
        }

        private async Task<string> ReadStreamFromResponse(WebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            using (StreamReader sr = new StreamReader(responseStream))
            {
                //Need to return this response 
                string strContent = await sr.ReadToEndAsync();
                return strContent;
            }
        }

        #endregion

        #region Helpers
        protected async Task<T> WithConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = _dbConnectionFactory.CreateConnection())
                {
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(String.Format("{0}.WithConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(String.Format("{0}.WithConnection() experienced a SQL exception (not a timeout)", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = _dbConnectionFactory.CreateConnection())
                {
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(String.Format("{0}.WithConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(String.Format("{0}.WithConnection() experienced a SQL exception (not a timeout)", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = _dbConnectionFactory.CreateConnection())
                {
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(String.Format("{0}.WithConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(String.Format("{0}.WithConnection() experienced a SQL exception (not a timeout)", GetType().FullName), ex);
            }
        }
        #endregion
        
    }
}
