﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using WeatherOutlookLondon.Models;

namespace WeatherOutlookLondon.Data.Interfaces
{
    public interface ICrudRepository<T> where T : class
    {
        Task<T> Find();
        Task<ICollection<T>> All();
        int GetAverageForPreceedingWeeks(Temperature temperature);
        Task<ICollection<T>> FindWhere(Expression<Func<T, bool>> predicate);
        Task Add(T obj);
        Task Update(T obj);
    }
}
