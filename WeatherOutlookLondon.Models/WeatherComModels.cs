﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherOutlookLondon.Models
{

    public class Head
    {
        public string locale { get; set; }
        public string form { get; set; }
        public string ut { get; set; }
        public string ud { get; set; }
        public string us { get; set; }
        public string up { get; set; }
        public string ur { get; set; }
    }

    public class Loc
    {
        [JsonProperty("id")]
        public string id { get; set; }
        public string dnam { get; set; }
        public string tm { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public string sunr { get; set; }
        public string suns { get; set; }
        public string zone { get; set; }
    }

    public class Bar
    {
        public string r { get; set; }
        public string d { get; set; }
    }

    public class Wind
    {
        public string s { get; set; }
        public string gust { get; set; }
        public string d { get; set; }
        public string t { get; set; }
    }

    public class Uv
    {
        public string i { get; set; }
        public string t { get; set; }
    }

    public class Moon
    {
        public string icon { get; set; }
        public string t { get; set; }
    }

    public class Cc
    {
        public string lsup { get; set; }
        public string obst { get; set; }
        public string tmp { get; set; }
        public string flik { get; set; }
        public string t { get; set; }
        public string icon { get; set; }
        public Bar bar { get; set; }
        public Wind wind { get; set; }
        public string hmid { get; set; }
        public string vis { get; set; }
        public Uv uv { get; set; }
        public string dewp { get; set; }
        public Moon moon { get; set; }
    }

    public class Wind2
    {
        public string s { get; set; }
        public string gust { get; set; }
        public string d { get; set; }
        public string t { get; set; }
    }

    public class Part
    {
        [JsonProperty("@p")]
        public string p { get; set; }
        public string icon { get; set; }
        public string t { get; set; }
        public Wind2 wind { get; set; }
        public string bt { get; set; }
        public string ppcp { get; set; }
        public string hmid { get; set; }
    }

    public class Day
    {
        [JsonProperty("@d")]
        public string d { get; set; }
        [JsonProperty("@t")]
        public string t { get; set; }
        [JsonProperty("@dt")]
        public string dt { get; set; }
        public string hi { get; set; }
        public string low { get; set; }
        public string sunr { get; set; }
        public string suns { get; set; }
        public List<Part> part { get; set; }
    }

    public class Dayf
    {
        public string lsup { get; set; }
        public List<Day> day { get; set; }
    }

    public class Weather
    {
        [JsonProperty("@ver")]
        public string ver { get; set; }
        public Head head { get; set; }
        public Loc loc { get; set; }
        public Cc cc { get; set; }
        public Dayf dayf { get; set; }
    }

    public class RootObject
    {
        public Weather weather { get; set; }
    }

}
