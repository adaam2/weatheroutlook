﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherOutlookLondon.Data.Factories;
using WeatherOutlookLondon.Data.Repositories;
using WeatherOutlookLondon.Models;
using Dapper;

namespace WeatherOutlookLondon.Data.Seed
{
    public class Seeder
    {
        private readonly string _connectionString;

        private static Random rnd = new Random();

        private ForecastRepository repo;

        public Seeder(string connectionString)
        {
            this._connectionString = connectionString;
            this.repo = new ForecastRepository(new SqlConnectionFactory(_connectionString));
        }

        public async Task SeedDatabaseAsync()
        {
            repo = new ForecastRepository(new SqlConnectionFactory(_connectionString));

            // Seed the db

            await ClearDatabaseAsync();

            // first seed database with real data for today, tomorrow and the day after
            var futureForecasts = await RetrieveFutureForecastsFromWeatherDotCom();

            foreach(Forecast f in futureForecasts)
            {
                await repo.Add(f);
            }

            // then do dummy data from the previous 28 days before today

            const int maxDaysToGoBack = 28;
            int counter = 0;

            var descriptions = new List<string>()
            {
                    "Partly cloudy",
                    "Sunny",
                    "Overcast",
                    "Rainy",
                    "Snowy"
            };

            DateTime yesterday = DateTime.Today.AddDays(-1);

            while (counter < maxDaysToGoBack)
            {

                int r = rnd.Next(descriptions.Count);

                DateTime currentDay = yesterday.AddDays(-counter);

                Forecast forecast = new Forecast()
                {
                    Date = currentDay,
                    DayName = currentDay.ToString("dddd"),
                    Description = descriptions[rnd.Next(0, descriptions.Count)],
                    HighTemp = RandomInRange(15, 25),
                    LowTemp = RandomInRange(5, 15)
                };

                await repo.Add(forecast);

                counter++;
            }
        }

        public async Task<ICollection<Forecast>> RetrieveFutureForecastsFromWeatherDotCom()
        {
            return await repo.RetrieveFutureForecasts();
        }

        private int RandomInRange(int minValue, int maxValue)
        {
            return rnd.Next(minValue, maxValue);
        }

        public async Task ClearDatabaseAsync()
        {
            const string sql = "DELETE FROM dbo.Forecast";

            using (var conn = new SqlConnectionFactory(_connectionString).CreateConnection())
            {
                await conn.ExecuteAsync(sql);
            }
        }
    }
}
