﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WeatherOutlookLondon.Configuration;
using WeatherOutlookLondon.Data.Factories;
using WeatherOutlookLondon.Data.Interfaces;
using WeatherOutlookLondon.Data.Repositories;
using WeatherOutlookLondon.Models;
using Dapper;
using System.Threading.Tasks;

namespace WeatherOutlookLondon
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["main"].ConnectionString;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            SetupDependencyInjection();
        }

        public void SetupDependencyInjection()
        {
            IKernel kernel = new StandardKernel();

            kernel.Bind<IDbConnectionFactory>()
            .To<SqlConnectionFactory>()
            .WithConstructorArgument("connectionString", _connectionString);

            kernel.Bind<ICrudRepository<Forecast>>().To<ForecastRepository>().WithConstructorArgument("dbConnectionFactory", new SqlConnectionFactory(_connectionString));

            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }     
    }
}
