﻿var highAverage = parseInt(document.getElementById("highAvg").innerHTML);
var lowAverage = parseInt(document.getElementById("lowAvg").innerHTML);

var Day = React.createClass({
  rawMarkup: function() {
    var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
    return { __html: rawMarkup };
  },

  render: function() {
    return (
      <div className="day">
        <h2 className="dayName">
            {this.props.dayName}

            <span className="date">
                {this.props.date}
            </span>
        </h2>
        <p className="description">
            {this.props.description}
        </p>
        <p className="highTemperature">
           <strong>High:</strong> {this.props.high}°
           <span className={ 'difference ' + (this.props.high > highAverage ? 'positive' : 'negative')}>
               {(() => {
               if (this.props.high > highAverage) {
                   return "+" + (this.props.high - highAverage) + "°";
               }
               else if (this.props.high < highAverage) {
                   return "-" + (highAverage - this.props.high) + "°";
               }
               else if (this.props.high === highAverage) {
                   return "";
               }
              })()}              
           </span>

        </p>
        <p className="lowTemperature">
           <strong>Low:</strong> {this.props.low}°
            <span className={ 'difference ' + (this.props.low > lowAverage ? 'positive' : 'negative')}>{(() => {
               if (this.props.low > lowAverage) {
                   return "+" + (this.props.low - lowAverage) + "°";
               }
               else if (this.props.low < lowAverage) {
                   return "-" + (lowAverage - this.props.low) + "°";
               }
               else if (this.props.low === lowAverage) {
                   return "";
               }
              })()}
            </span>

        </p>
      </div>
    );
  }
});

var ForecastWidget = React.createClass({
  loadForecastsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
      this.loadForecastsFromServer();
    setInterval(this.loadForecastsFromServer, this.props.pollInterval);
  },
  render: function() {
    return (
      <div className="forecastWidget">
        <h1>Forecasts</h1>
        <ForecastList data={this.state.data} />
      </div>
    );
  }
});

var ForecastList = React.createClass({
  render: function() {
    var forecastNodes = this.props.data.map(function(day) {
      return (
        <Day dayName={day.DayName} date={day.DateStr } description={ day.Description } high={ day.HighTemp } low={ day.LowTemp }>{day}
        </Day>
      );
    });
    return (
      <div className="dayList">{forecastNodes}
      </div>
    );
  }
});

ReactDOM.render(
  <ForecastWidget url="/forecast" pollInterval={2000000} />,
  document.getElementById('content')
);